Studio Description
======

Studio is a base theme pack that focuses on clean implementation of a custom theme. To accomplish this, the theme provides the following functionality:

* Keep as much functionality as possible out of the <code>*.tpl.php</code> files.
* Add support for building tag attributes for most anything in preprocess.
* Break the template.php into multiple files to keep it from getting unmanageable.
* Build in functionality for adding IE specific stylesheets.
* Add support for moving CSS files to the top of the stack. (i.e. - <code>reset.css</code>, <code>960.css</code>)

Implementation
=======

To use the Studio functionality, simply base your own custom theme off of the Canvas theme. This is done by adding the following to your theme's .info file:

<code>base theme = canvas</code>

Please note that Canvas is a very plain and minimalist theme. If you would like to add some instant formatting and style to your theme, use Paint instead:

<code>base theme = paint</code>
