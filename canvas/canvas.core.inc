<?php

/**
 * Collect all information for the active theme.
 */
function _canvas_theme_collector() {
  $themes = list_themes();
  $themes_active = array();
  global $theme_info;

  // If there is a base theme, collect the names of all themes that may have
  // data files to load.
  if (isset($theme_info->base_theme)) {
    global $base_theme_info;
    foreach ($base_theme_info as $base){
      $themes_active[$base->name] = $themes[$base->name];
      $themes_active[$base->name]->path = drupal_get_path('theme', $base->name);
    }
  }

  // Add the active theme to the list of themes that may have data files.
  $themes_active[$theme_info->name] = $themes[$theme_info->name];
  $themes_active[$theme_info->name]->path = drupal_get_path('theme', $theme_info->name);

  return $themes_active;
}

/**
 * Collect paths for all the include files. The resulting array will have all
 * file paths to include keyed by the directory they reside in.
 */
function _canvas_include_files_collector($directory_name, $root_path, $cached = TRUE) {
  // This funtion relies on A proper path to look into, so if they are not
  // available return nothing.
  if (empty($directory_name) || empty($root_path)) {
    return '';
  }

  // Open the directory and recursively scan for all *.inc files.
  $directory = opendir($root_path . '/' .  $directory_name);
  $entry_array = array();
  while($entry_name = readdir($directory)) {
    if ($entry_name != '.' && $entry_name != '..' && !is_dir($root_path . '/' .  $directory_name . '/' . $entry_name)) {
      $ext = substr($entry_name, strrpos($entry_name, '.') + 1);
      if ($ext == 'inc') {
        $entry_array[$directory_name] = $root_path . '/' .  $directory_name . '/' . $entry_name;
      }
    }
    elseif ($entry_name != '.' && $entry_name != '..' && is_dir($root_path . '/' .  $directory_name . '/' . $entry_name)) {
      $entry_array = array_merge($entry_array, _canvas_include_files_collector($entry_name, $root_path . '/' .  $directory_name, FALSE));
    }
  }
  closedir($directory);

  return $entry_array;
}

/**
 * Load the include files.
 */
function _canvas_load_files($includes = array()) {
  foreach ($includes as $path) {
    if (is_array($path)) {
      _canvas_load_files($path);
    }
    else {
      require $path;
    }
  }
}

/**
 * Initialize the Canvas system by finding all relevant information on themes
 * and then including the needed files.
 */
function _canvas_init() {
  // Check the cache. If it is empty, rebuild it.
  if ($cache = cache_get('theme_registry:studio:includes')) {
    $paths = $cache->data;
  }
  else {
    // Get the files to be included.
    $themes = _canvas_theme_collector();
    $paths = array();
    foreach ($themes as $name => $theme) {
      if (isset($theme->info['include directories'])) {
        foreach ($theme->info['include directories'] as $directory) {
          $paths[$name] = _canvas_include_files_collector($directory, $theme->path);
        }
      }
    }

    cache_set('theme_registry:studio:includes', $paths);
  }

  // Include the files.
  foreach ($paths as $theme_paths) {
    _canvas_load_files($theme_paths);
  }
}

/**
 * Collect the hooks to be processed. Some modules (like Views have parent
 * template and preprocess files for the hooks.
 */
function _canvas_hook_collector($hook) {
  $hooks = array();
  $subhook_position = strpos($hook, '__');

  if ($subhook_position !== FALSE) {
    $hook_pieces = explode('__', $hook);
    $hooks[] = array_shift($hook_pieces);
    foreach ($hook_pieces as $piece) {
      $last_hook = count($hooks) - 1;
      $hooks[] = $hooks[$last_hook] . '__' . $piece;
    }
  }

  $hooks[] = $hook;

  return $hooks;
}

/**
 * Cycle through the Studio attributes for all themes and elements and crunch
 * them into strings.
 */
function _canvas_process_attributes($attributes_array = array()) {
  $attributes = array();
  if (is_array($attributes_array)) {
    foreach ($attributes_array as $element => $data) {
      $attributes[$element] = drupal_attributes($data);
    }
  }
  return $attributes;
}

/**
 * Check for stylesheets to be placed at the top of the stack or conditional
 * Internet Explorer styles in the .info file and add them to the $styles
 * variable.
 */
function _canvas_process_stylesheets($themes_active) {
  // Prepare the needed variables.
  global $theme_info;
  $framework_styles = array();
  $conditional_styles = array();

  // If there is more than one active theme, check all base themes for
  // stylesheets.
  if (count($themes_active) > 1) {
    global $base_theme_info;
    foreach ($base_theme_info as $name => $info) {
      if (isset($info->info['framework stylesheets'])) {
        $framework_styles[$name] = $info->info['framework stylesheets'];
      }
      if (isset($info->info['conditional stylesheets'])) {
        $conditional_styles[$name] = $info->info['conditional stylesheets'];
      }
    }
  }

  // Check the current theme for stylesheets.
  if (isset($theme_info->info['framework stylesheets'])) {
    $framework_styles[$theme_info->name] = $theme_info->info['framework stylesheets'];
  }
  if (isset($theme_info->info['conditional stylesheets'])) {
    $conditional_styles[$theme_info->name] = $theme_info->info['conditional stylesheets'];
  }

  // If there is at least one entry in the $framework_styles array, process it.
  if (count($framework_styles) >= 1) {
    // Add all the framework stylesheets to a group so they are loaded first.
    foreach ($framework_styles as $theme => $medias) {
      foreach ($medias as $media => $stylesheets) {
        foreach ($stylesheets as $path) {
          $path = drupal_get_path('theme', $theme) . '/' . $path;
          drupal_add_css($path, array(
            'group' => CSS_SYSTEM,
            'media' => $media,
            'weight' => -1000,
            'every_page' => TRUE,
          ));
        }
      }
    }
  }

  // If there is at least one entry in the $conditional_styles array, process it.
  if (count($conditional_styles) >= 1) {
    // Add all the conditional stylesheets with drupal_add_css().
    foreach ($conditional_styles as $theme => $conditions) {
      foreach ($conditions as $condition => $medias) {
        foreach ($medias as $media => $stylesheets) {
          foreach ($stylesheets as $path) {
            $path = drupal_get_path('theme', $theme) . '/' . $path;
            if ($condition == '!ie') {
              $browsers = array('!IE' => TRUE, 'IE' => FALSE);
            }
            else {
              $browsers = array('!IE' => FALSE, 'IE' => $condition);
            }
            drupal_add_css($path, array(
              'media' => $media,
              'every_page' => TRUE,
              'browsers' => $browsers,
            ));
          }
        }
      }
    }
  }
}
