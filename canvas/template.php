<?php

/**
 * Load the helper functions.
 */
require 'canvas.core.inc';

/**
 * Initialize the canvas system.
 */
_canvas_init();
