<?php

/**
 * Implementation of hook_preprocess().
 *
 * This will initiate the primary functions of Studio. Namely the attributes
 * creation and preprocess file loading.
 */
function canvas_preprocess(&$vars, $hook) {
  $themes_active = _canvas_theme_collector();
  $hooks = _canvas_hook_collector($hook);

  // Prepare that Studio attributes array and merge in all the predefined Drupal
  // attribute arrays.
  if(!isset($vars['studio_attributes_array'])) {
    $vars['studio_attributes_array'] = array();

    if (isset($vars['attributes_array'])) {
      $vars['studio_attributes_array'][$hooks[0]] = $vars['attributes_array'];
    }

    if (isset($vars['classes_array'])) {
      $vars['studio_attributes_array'][$hooks[0]]['class'] = $vars['classes_array'];
    }

    if (isset( $vars['title_attributes_array'])) {
      $vars['studio_attributes_array']['title'] =  $vars['title_attributes_array'];
    }

    if (isset($vars['content_attributes_array'])) {
      $vars['studio_attributes_array']['content'] = $vars['content_attributes_array'];
    }
  }
  else {
    if (isset($vars['attributes_array'])) {
      $vars['studio_attributes_array'][$hooks[0]] = array_merge_recursive($vars['studio_attributes_array'][$hooks[0]], $vars['attributes_array']);
    }

    if (isset($vars['classes_array'])) {
      $vars['studio_attributes_array'][$hooks[0]]['class'] = array_merge($vars['studio_attributes_array'][$hooks[0]]['class'], $vars['classes_array']);
    }

    if (isset( $vars['title_attributes_array'])) {
      $vars['studio_attributes_array']['title'] = array_merge_recursive($vars['studio_attributes_array']['title'], $vars['title_attributes_array']);
    }

    if (isset($vars['content_attributes_array'])) {
      $vars['studio_attributes_array']['content'] = array_merge_recursive($vars['studio_attributes_array']['content'], $vars['content_attributes_array']);
    }
  }
}

/**
 * Implementation of hook_process().
 *
 * This will finalize any changes done in preprocess functions.
 */
function canvas_process(&$vars, $hook) {
  $themes_active = _canvas_theme_collector();
  $vars['studio_attributes'] = _canvas_process_attributes($vars['studio_attributes_array']);
}
