<?php

/**
 * Implementation of hook_preprocess_hook().
 */
function canvas_preprocess_html(&$variables, $hook) {
  // Process the extra stylesheets from this and other subthemes.
  _canvas_process_stylesheets(_canvas_theme_collector());

  // Replace existing attributes in html.tpl.php with entries in
  // $studio_attributes.

  // Skip link
  //
  // Adding the href here will make it possible to change it if a different page
  // structure is used without having to override the tpl again.
  $variables['studio_attributes_array']['skip_link_container']['id'] = "skip-link";
  $variables['studio_attributes_array']['skip_link']['href'] = "#main-content";
}
